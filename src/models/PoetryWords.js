const { Model, DataTypes } = require('sequelize');


class PoetryWords extends Model {
    static init(sequelize) {
        super.init({
            poetryId: {
                field:'poetry_id',
                type: DataTypes.INTEGER,
                allowNull: false
            },
            wordId: {
                field:'word_id',
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },{
            tableName: 'poetry_words',
            sequelize
        })
    }
    static associate({Poetry,Words}){
        this.belongsTo(Poetry,{
            foreignKey:'poetry_id',
            as:'poetry'
        })
        this.belongsTo(Words,{
            foreignKey:'word_id',
            as:'words'
        })
    }
}


module.exports = PoetryWords;