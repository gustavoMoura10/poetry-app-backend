const { Model, DataTypes } = require('sequelize');
const Poetry = require('./Poetry');
const PoetryWords = require('./PoetryWords');


class Words extends Model {
    static init(sequelize) {
        super.init({
            name: {
                type: DataTypes.STRING,
                allowNull: false
            }
        }, {
            tableName: 'words',
            sequelize
        })
    }
    static associate({ PoetryWords, Poetry }) {
        this.belongsToMany(Poetry, {
            through: PoetryWords,
            as:'poetry',
            foreignKey:'word_id'
        });
    }
}



module.exports = Words;
