const { Model, DataTypes } = require('sequelize');
class Poetry extends Model {
    static init(sequelize) {
        super.init({
            title: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            content: {
                type: DataTypes.STRING,
                allowNull: false
            },
            userId: {
                type: DataTypes.INTEGER,
                field: 'user_id',
                allowNull: false,
                references: {
                    model: 'users', key: 'id'
                },
            },
        }, {
            tableName: 'poetry',
            sequelize
        })
    }
    static associate({ Users, Words, PoetryWords }) {
        this.belongsTo(Users)
        this.belongsToMany(Words, {
            through: PoetryWords,
            as:'words',
            foreignKey:'poetry_id'
        });
    }
}


module.exports = Poetry;
