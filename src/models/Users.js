const { Model, DataTypes } = require('sequelize');
const bcryptjs = require("bcryptjs");
const Poetry = require('./Poetry');

class Users extends Model {
    static init(sequelize) {
        super.init({
            firstName: {
                field: 'first_name',
                type: DataTypes.STRING,
                allowNull: false
            },
            lastName: {
                field: 'last_name',
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,

            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,

            },
            admin: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            }
        }, {
            tableName: 'users',
            sequelize
        })
    }
    static associate({ Poetry , RefreshToken}) {
        this.hasMany(Poetry, {
            as: 'poetry',
            foreignKey: 'user_id',
        });
        this.hasOne(RefreshToken,{
            as: 'refreshToken',
            foreignKey: 'user_id',

        })
    }
    static hooks() {
        this.beforeCreate(async function (user) {
            const salt = bcryptjs.genSaltSync(10);
            user.password = bcryptjs.hashSync(user.password, salt)
        })
    }
}

module.exports = Users;
