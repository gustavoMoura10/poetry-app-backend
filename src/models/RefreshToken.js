const { Model, DataTypes } = require('sequelize');


class RefreshToken extends Model {
    static init(sequelize) {
        super.init({
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            token: {
                type: DataTypes.STRING(1000),
                allowNull: false,
            },
            userId: {
                field: 'user_id',
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'users', key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
            }
        }, {
            tableName: 'refresh_token',
            sequelize
        })
    }
    static associate({ Users }) {
        this.belongsTo(Users, {
            foreignKey: 'user_id',
            as: 'user'
        })
    }
}


module.exports = RefreshToken;
