const { Router } = require('express');
const { login } = require("../controllers/authController");
const validateSchema = require('../middlewares/validateSchema');
const { loginSchema } = require('../schemas/authSchemas');
const router = Router();

router.post('/login', validateSchema(loginSchema, 'body'), login);

module.exports = router;
