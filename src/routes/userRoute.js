const { Router } = require('express');
const { saveUser} = require('../controllers/userController');
const validateSchema = require('../middlewares/validateSchema');
const { saveUserSchema, loginSchema } = require('../schemas/userSchemas');

const router = Router()

router.post('/saveUser', validateSchema(saveUserSchema, 'body'), saveUser);


module.exports = router
