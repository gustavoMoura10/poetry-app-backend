const Joi = require('joi');
const validateSchema = (schema, property) => {
    return async (req, res, next) => {
        try {
            const value = await schema.validateAsync(req[property]);
            next();
        } catch (error) {
            const { details } = error;
            const message = details.map(i => i.message).join(',')
            console.log("error", message);
            res.status(422).send({ error: true, message })
        }
    }
}
module.exports = validateSchema;
