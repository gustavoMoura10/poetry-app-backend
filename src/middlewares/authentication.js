const { response } = require("express");

const athentication = (req, res, next) => {
    const auth = req.headers.authorization;
    const id = req.headers.authorization_id;
    if (!auth) {
        return res.status(401).send({ error: true, message: 'Missing authorization' })
    }
    if (!id) {
        return res.status(401).send({ error: true, message: 'Missing authorization id' })
    }
    if (`${auth}`.startsWith('Bearer')) {
        return res.status(401).send({ error: true, message: 'Wrong authorization' })
    }
    let token = auth.split(/Bearer/g).pop().trim()
}
