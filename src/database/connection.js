const path = require('path');
const fs = require('fs');
const { Sequelize } = require('sequelize');
const databaseConfig = require('../config/databaseConfig');

const connection = new Sequelize(databaseConfig);

const modelsPath = path.resolve(__dirname, '..', 'models');
const models = fs.readdirSync(modelsPath).reduce((obj, file) => {
    let name = file.split('.').shift();
    obj[name] = require(path.resolve(modelsPath, file));
    return obj;
}, {});


exports.getConnection = () => {
    for (let model of Object.values(models)) {
        model.init(connection)
    }
    for (let model of Object.values(models)) {
        if (typeof model.associate === 'function') {
            model.associate(models)
        }
        if (typeof model.hooks === 'function') {
            model.hooks()
        }
    }
    connection.sync();

    return connection;
}