const Joi = require('joi');

exports.saveUserSchema = Joi.object({
    firstName: Joi.string().alphanum().max(255).required(),
    lastName: Joi.string().alphanum().max(255).required(),
    email: Joi.string().email().max(255).required(),
    password: Joi.string().min(8).max(25).required(),
    confirmPassword: Joi.string().required().valid(Joi.ref('password'))
})


