const Joi = require('joi');
exports.loginSchema = Joi.object({
    email: Joi.string().email().max(255).required(),
    password: Joi.string().min(8).max(25).required()
})
