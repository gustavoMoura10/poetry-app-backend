const { Op } = require('sequelize');
const Users = require('../models/Users')
const bcrypt = require('bcryptjs');

exports.saveUser = async (req, resp, next) => {
    let status = 400;
    try {
        let { email, firstName, lastName, password } = req.body;
        let findUser = await Users.findOne({
            where: {
                email: {
                    [Op.eq]: email,
                }
            }
        });
        if (!!findUser) {
            return res.status(400).send({ error: true, message: 'User already exists' })
        }
        status = 500;
        const user = await (await Users.create({
            email, firstName, lastName, password
        })).toJSON();
        delete user.password;
        status = 200;
        return resp.status(status).json(user);
    } catch (error) {
        console.log(error);
        return resp
            .status(status)
            .send(
                status === 500 ? { error: true, message: "Error in Server" } : error
            );

    }
}


