const Users = require('../models/Users')
const RefreshToken = require('../models/RefreshToken')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Op } = require('sequelize');
const expiresIn = '365d';
exports.login = async (req, resp, next) => {
    let status = 400;
    try {
        const { email, password } = req.body;
        const user = await (await Users.findOne({
            where: {
                email: { [Op.eq]: email }
            }
        })).toJSON();
        if (!user) {
            return res.status(status).send({ error: true, message: 'User not found' })
        }
        let validatePassword = await bcrypt.compareSync(password, user.password);
        if (!validatePassword) {
            return res.status(status).send({ error: true, message: 'Invalid password' })
        }
        status = 500;
        delete user.password;
        const token = jwt.sign({
            ...user
        }, process.env.JWT_TOKEN, { expiresIn});
        let findToken = await (await RefreshToken.findOne({
            where: {
                userId: { [Op.eq]: user.id }
            }
        })).toJSON();
        if (findToken) {
            try {
                const verify = jwt.verify(findToken.token, process.env.JWT_TOKEN);


                if (verify) {
                    status = 200;
                    return resp.status(status).json({
                        ...user,
                        refreshToken:findToken
                    })
                }

            } catch (error) {
                await RefreshToken.update({
                    token
                },
                    {
                        where: {
                            userId: { [Op.eq]: user.id }
                        }
                    })
                findToken = await (await RefreshToken.findOne({
                    where: {
                        userId: { [Op.eq]: user.id }
                    }
                })).toJSON();
                status = 200;
                return resp.status(status).json({
                    ...user,
                    refreshToken:findToken
                })

            }
        }
        let refreshToken = await (await RefreshToken.create({
            userId: user.id,
            token
        })).toJSON();
        status = 200;
        return resp.status(status).json({
            ...user,
            refreshToken
        })

    } catch (error) {
        console.log(error);
        return resp
            .status(status)
            .send(
                status === 500 ? { error: true, message: "Error in Server" } : error
            );

    }

}
