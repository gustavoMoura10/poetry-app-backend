const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const { getConnection } = require('./database/connection');
const app = express();
const router = require('./router');
(async () => {
    try {
        app.use(express.json())
        await getConnection().validate();
        router(app)
        app.listen(process.env.PORT, () => {
            console.log(`RUNNING ON PORT:`, process.env.PORT)
        })
    } catch (error) {
        console.log(error)
    }
})();
