let userRoute = require('./routes/userRoute');
let poetryRoute = require('./routes/poetryRoute');
let authRoute = require('./routes/authRoute');
module.exports = (app) => {
    app.use('/auth', authRoute)
    app.use('/users', userRoute)
    app.use('/poetry', poetryRoute)
}
